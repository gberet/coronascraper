﻿using System;
using System.Linq;

namespace CoronaScraper
{
  class Program
  {
    static void Main(string[] args)
    {
      var s = new Scraper();
      var dataZemi = s.Scrapovat();
      Console.WriteLine(CountryData.FormatString, "Zeme", "Total", "Deaths", "Rate [%]");
      dataZemi.ForEach(Console.WriteLine);
    }
  }
}
